msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2017-01-24 17:28+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr ""

#. (itstool) path: info/desc
#: C/index.page:7
msgid "Help for the Lollypop music player."
msgstr ""

#. (itstool) path: info/title
#: C/index.page:8
msgctxt "text"
msgid "Lollypop music player"
msgstr ""

#. (itstool) path: credit/name
#: C/index.page:11
msgid "Cédric Bellegarde"
msgstr ""

#. (itstool) path: credit/years
#: C/index.page:13
msgid "2015"
msgstr ""

#. (itstool) path: page/title
#: C/index.page:18
msgid "<_:media-1/> Lollypop music player"
msgstr ""

#. (itstool) path: section/title
#: C/index.page:21
msgid "Add music"
msgstr ""

#. (itstool) path: section/p
#: C/index.page:22
msgid "If Lollypop fails to detect your music:"
msgstr ""

#. (itstool) path: item/p
#: C/index.page:24
msgid "Check that either gstreamer-libav or gstreamer-fluendo is installed"
msgstr ""

#. (itstool) path: item/p
#: C/index.page:25
msgid "Go to the application menu: \"<sys>Settings -&gt; Music</sys>\" and add your music folder"
msgstr ""

#. (itstool) path: section/title
#: C/index.page:30
msgid "Playback"
msgstr ""

#. (itstool) path: section/p
#: C/index.page:31
msgid "When you start playing a song, a contextual playlist is populated."
msgstr ""

#. (itstool) path: section/p
#: C/index.page:32
msgid "Contexts can be: an album, an artist, a genre, popular or recent albums, searches, playlists, …"
msgstr ""

#. (itstool) path: section/p
#: C/index.page:33
msgid "Shuffle interacts on current context."
msgstr ""

#. (itstool) path: section/title
#: C/index.page:37
msgid "Rate albums"
msgstr ""

#. (itstool) path: section/p
#: C/index.page:38
msgid "Album popularity is based on how many tracks from an album you listen to."
msgstr ""

#. (itstool) path: section/p
#: C/index.page:39
msgid "Album rating will be influenced by album popularity. In other words, rate will decrease if you stop listening to an album."
msgstr ""

#. (itstool) path: p/link
#: C/legal.xml:4
msgid "Creative Commons Attribution-Share Alike 3.0 United States License"
msgstr ""

#. (itstool) path: license/p
#: C/legal.xml:3
msgid "This work is licensed under a <_:link-1/>."
msgstr ""

#. (itstool) path: license/p
#: C/legal.xml:6
msgid "As a special exception, the copyright holders give you permission to copy, modify, and distribute the example code contained in this document under the terms of your choosing, without restriction."
msgstr ""

